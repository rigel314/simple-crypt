package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime/debug"
)

var (
	newKey  = flag.Bool("newkey", false, "pass to create a new key")
	encrypt = flag.Bool("encrypt", false, "pass to encrypt")
	decrypt = flag.Bool("decrypt", false, "pass to decrypt")

	cryptKeyFile = flag.String("keyfile", "", "encryption/decryption key file path")
	ivFile       = flag.String("ivfile", "", "iv file path")
	iFile        = flag.String("ifile", "", "output file path")
	oFile        = flag.String("ofile", "", "output file path")
	keyType      = flag.String("type", "aes256gcm", "encryption type")
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	bi, ok := debug.ReadBuildInfo()
	if !ok {
		log.Println("version: uncontrolled")
	} else {
		msg := "version: uncontrolled"
		for _, v := range bi.Settings {
			if v.Key == "vcs.revision" {
				msg = "version: " + v.Value
			}
		}
		log.Println(msg)
	}
	flag.Parse()

	switch {
	case *newKey:
		sz := int64(0)
		switch *keyType {
		case "aes256gcm":
			sz = 32 // 256-bit is 32-byte
		default:
			log.Fatal("invalid keytype:", *keyType)
		}

		err := randFile(*cryptKeyFile, sz)
		if err != nil {
			log.Fatal(err)
		}

	case *encrypt:
		ck, err := readAll(*cryptKeyFile)
		if err != nil {
			log.Fatal("failed to open key file:", *cryptKeyFile, err)
		}

		inf, err := os.Open(*iFile)
		if err != nil {
			log.Fatal("failed to open input file:", *iFile, err)
		}
		defer inf.Close()
		outf, err := os.OpenFile(*oFile, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
		if err != nil {
			log.Fatal("failed to create output file:", *oFile, err)
		}
		defer outf.Close()

		switch *keyType {
		case "aes256gcm":
			block, err := aes.NewCipher(ck)
			if err != nil {
				log.Fatal("failed to create aes cipher:", err)
			}

			ivf, err := os.OpenFile(*ivFile, os.O_WRONLY|os.O_CREATE, 0600)
			if err != nil {
				log.Fatal("failed to open iv file:", *ivFile, err)
			}
			defer ivf.Close()

			ivRd := io.TeeReader(rand.Reader, ivf)
			ivBuf := make([]byte, 12)
			buf := make([]byte, 60000)
			lenBuf := make([]byte, 2)
			seqBuf := make([]byte, 8)
			var seq uint64

			for {
				n, err := io.ReadFull(inf, buf)
				if err != nil && err != io.ErrUnexpectedEOF {
					if err == io.EOF {
						break
					}
					log.Fatal("failed to read input file:", *iFile, err)
				}

				_, err = io.ReadFull(ivRd, ivBuf)
				if err != nil {
					log.Fatal("failed to read iv:", *cryptKeyFile, err)
				}

				aesGcm, err := cipher.NewGCM(block)
				if err != nil {
					log.Fatal("failed to create gcm: ", err)
				}

				binary.LittleEndian.PutUint64(seqBuf, seq)

				cTxt := aesGcm.Seal(nil, ivBuf, buf[:n], seqBuf)

				binary.LittleEndian.PutUint16(lenBuf, uint16(len(cTxt)))

				n, err = outf.Write(lenBuf)
				if err != nil {
					log.Fatal("failed to write output file", *oFile, err)
				}
				if n != len(lenBuf) {
					log.Fatal("failed to write all bytes to output file", *oFile, "wrote", n, "expected", len(lenBuf))
				}

				n, err = outf.Write(cTxt)
				if err != nil {
					log.Fatal("failed to write output file", *oFile, err)
				}
				if n != len(cTxt) {
					log.Fatal("failed to write all bytes to output file", *oFile, "wrote", n, "expected", len(cTxt))
				}
			}

		default:
			log.Fatal("invalid keytype:", *keyType)
		}

	case *decrypt:
		ck, err := readAll(*cryptKeyFile)
		if err != nil {
			log.Fatal("failed to open key file:", *cryptKeyFile, err)
		}

		inf, err := os.Open(*iFile)
		if err != nil {
			log.Fatal("failed to open input file:", *iFile, err)
		}
		defer inf.Close()
		outf, err := os.OpenFile(*oFile, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
		if err != nil {
			log.Fatal("failed to create output file:", *oFile, err)
		}
		defer outf.Close()

		switch *keyType {
		case "aes256gcm":
			block, err := aes.NewCipher(ck)
			if err != nil {
				log.Fatal("failed to create aes cipher:", err)
			}

			ivf, err := os.Open(*ivFile)
			if err != nil {
				log.Fatal("failed to open iv file:", *ivFile, err)
			}
			defer ivf.Close()

			ivBuf := make([]byte, 12)
			buf := make([]byte, 65536)
			lenBuf := make([]byte, 2)
			seqBuf := make([]byte, 8)
			var seq uint64

			for {
				_, err := io.ReadFull(inf, lenBuf)
				if err != nil {
					if err == io.EOF {
						break
					}
					log.Fatal("failed to read input file:", *iFile, err)
				}
				n, err := io.ReadFull(inf, buf[:binary.LittleEndian.Uint16(lenBuf)])
				if err != nil {
					log.Fatal("failed to read input file:", *iFile, err)
				}

				_, err = io.ReadFull(ivf, ivBuf)
				if err != nil {
					log.Fatal("failed to read iv:", *cryptKeyFile, err)
				}

				aesGcm, err := cipher.NewGCM(block)
				if err != nil {
					log.Fatal("failed to create gcm:", err)
				}

				binary.LittleEndian.PutUint64(seqBuf, seq)

				pTxt, err := aesGcm.Open(nil, ivBuf, buf[:n], seqBuf)
				if err != nil {
					log.Fatal("failed gcm decrypt:", err)
				}

				outf.Write(pTxt)
			}

		default:
			log.Fatal("invalid keytype:", *keyType)
		}

	default:
		log.Fatal("invalid command line")
	}
}

func randFile(path string, sz int64) error {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("failed to create new key file %q %w", path, err)
	}
	defer f.Close()

	rd := rand.Reader

	n, err := io.CopyN(f, rd, sz)
	if err != nil {
		return fmt.Errorf("failed to write random data to new key file %q %w", path, err)
	}
	if n != sz {
		return fmt.Errorf("failed to write n bytes random data to new key file %q, wrote %d, expected %d", path, n, sz)
	}

	return nil
}

func readAll(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open key file %q %w", path, err)
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("failed to read key file %q %w", path, err)
	}

	return b, nil
}
